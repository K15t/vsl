# Very Simple Licensing system
The Very Simple Licensing system (VSL) provides custom licensing support for any kind of apps.

The project is divided into several modules:

* **vsl-base**: contains the basic classes used by all integrations.
* **vsl-confluence**: contains helper classes and a generic license management UI for use in Confluence apps.
* **vsl-creator**: contains a key pair generator and a very basic tool for creating new licenses.

## Concepts

Concept | Description
--------- | ---------
License (file / string) | Contains some metadata (owner, what product it covers, timestamps) as well as policies that will be checked when the license is validated against a host product. There are multiple representations: `License` is intended for use in apps using VSL licensing, whereas `UnsignedLicense` and `SignedLicense` are only used internally and when creating new licenses.
Host Product | A product like Confluence or JIRA where a VSL-licensed app is installed into.
Policy | A type of check and respective parameters that need to be valid for the license to be considered valid. Example: license expiration at 2020-09-30
Private / Public Key | VSL uses RSA key pairs to sign licenses and verify them at runtime. The public key should be shipped by the app for later verification of licenses. The private key is only required when creating new licenses and must never be included in a licensed app.
License Store | A component that is responsible for storing and accessing licenses that users install. There are several implementations, for example backed by the filesystem or Atlassian's Bandana persistence layer.


## Integration into Atlassian apps
This section handles Confluence apps, but integration for other apps should work similar.

### SimpleLicenseService

The `SimpleLicenseService` is the central class for managing licenses in a VSL-licensed app. The first step of any integration should be to set up this service.

It's recommended to add it to the app's spring context for easy use in other parts of the app where license checks need to be done. This can be done using a Spring `FactoryBean` as follows:
```java
import com.atlassian.bandana.BandanaManager;
import com.k15t.vsl.base.KeyUtil;
import com.k15t.vsl.base.SimpleLicenseService;
import com.k15t.vsl.confluence.ConfluenceLicenseServiceFactory;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.security.spec.RSAPublicKeySpec;
import java.time.Instant;


@Component
public class SimpleLicenseServiceFactoryBean implements FactoryBean<SimpleLicenseService> {

    // This is the public key spec produced by the KeyGenerator from VSL Creator.
    // Must belong to the private key that is used to sign licenses for this app.
    private static final RSAPublicKeySpec PUBLIC_KEY_SPEC = new RSAPublicKeySpec(
            new BigInteger("..."),
            new BigInteger("...")
    );

    @Autowired private BandanaManager bandanaManager;


    @Override
    public SimpleLicenseService getObject() {
        return ConfluenceLicenseServiceFactory.withBandanaLicenseStore(
                bandanaManager,
                "com.k15t.examples.vsl-demo-app",
                Instant.parse("2020-01-15T00:00:00Z"),
                KeyUtil.createPublicKey(PUBLIC_KEY_SPEC)
        );
    }


    @Override
    public Class<?> getObjectType() {
        return SimpleLicenseService.class;
    }


    @Override
    public boolean isSingleton() {
        return true;
    }

}
```
In this case we've set up a `SimpleLicenseService` for the app with plugin key `com.k15t.examples.vsl-demo-app` that is backed by a Bandana 
based license store.
The build date (2020-01-15 00:00:00) is used for validating a license's maintenance expiration policy. If an app only uses a subscription model for licenses (expiration policy) this date will not be used.
The plugin key is checked when validating the license, so it is not possible to install a license for another app.
The public key should be hard-coded in the app and will be used to verify license signatures.

Once the SimpleLicenseService has been set up licenses checks can be added to the app like this:
```java
public class LicenseCheckExamples {

    @Autowired private SimpleLicenseService licenseService;


    public void validateLicense() throws LicenseException {
        License license = licenseService.getLicense()
                .orElseThrow(() -> new IllegalStateException("No license installed."));
        try {
            license.validatePolicies();
        } catch (LicensePolicyException e) {
            throw new IllegalStateException("Installed license is not valid: " + e.getMessage());
        }
    }


    public boolean isNearExpirationDate() {
        return getExpirationDate()
                .map(expirationDate -> LocalDate.now().plusMonths(1).isAfter(expirationDate))
                .orElse(false);
    }


    public Optional<LocalDate> getExpirationDate() {
        return licenseService.getLicense()
                .map(license -> license.getPolicy(ExpirationPolicy.class))
                .map(ExpirationPolicy::getExpiryDate);
    }
}
```

#### Using Multiple Signing Keys
Sometimes it's necessary to replace the key pair for license signing and signature verification, for example if the private key is no longer accessible, if it has been leaked to the public, or if regulations demand it.

To handle these more complex cases, VSL provides alternate APIs receiving a custom `LicenseVerifier` (instead of a `PublicKey`) which can be used if the app needs more control over the license verification and key selection:
```java
public class SimpleLicenseServiceFactoryBean {
    
    private static PublicKey OLD_KEY = KeyUtil.createPublicKey(OLD_KEY_SPEC);
    private static PublicKey NEW_KEY = KeyUtil.createPublicKey(NEW_KEY_SPEC);

    @Override
    public SimpleLicenseService getObject() {
        return ConfluenceLicenseServiceFactory.withBandanaLicenseStore(
                bandanaManager,
                "com.k15t.examples.vsl-demo-app",
                Instant.parse("2020-01-15T00:00:00Z"),
                signedLicense -> {
                    if (signedLicense.getLicenseInfo().getCreation().isBefore(LocalDate.of(2022, 8, 1))) {
                        signedLicense.verifySignature(OLD_KEY);
                    } else {
                        signedLicense.verifySignature(NEW_KEY);
                    }
                }
        );
    }
}
```
In this example the code uses different keys for signature verification based on the creation date of the license.
### License Management UI

The **vsl-confluence** module contains a reusable UI for managing VSL licenses.
It can be added to the app by following the steps below.

#### Adding Xwork Actions
First new xwork actions must be added to `atlassian-plugin.xml`:
```xml
    <xwork key="configuration-ui" name="Configuration UI">
        <package name="${project.artifactId}-configuration" extends="default" namespace="/admin/plugins/${project.artifactId}">
            <default-interceptor-ref name="validatingStack"/>

            <action name="license" class="com.k15t.examples.VslDemoLicenseAction">
                <interceptor-ref name="defaultStack"/>
                <result name="success" type="velocity">/com/k15t/vsl/confluence/license-ui.vm</result>
                <result name="error" type="velocity">/com/k15t/vsl/confluence/license-ui.vm</result>
                <param name="selected-web-item-key">${project.artifactId}-license-item</param>
                <param name="i18n-prefix">vsl-demo</param>
            </action>

            <action name="doupdatelicense" class="com.k15t.examples.VslDemoLicenseAction" method="doUpdate">
                <param name="RequireSecurityToken">true</param>
                <result name="success" type="redirect">license.action</result>
                <result name="input" type="velocity">/com/k15t/vsl/confluence/license-ui.vm</result>
                <result name="error" type="velocity">/com/k15t/vsl/confluence/license-ui.vm</result>
                <param name="selected-web-item-key">${project.artifactId}-license-item</param>
                <param name="i18n-prefix">vsl-demo</param>
            </action>
        </package>
    </xwork> 
```
The referenced action class must be created within the licensed app by subclassing `AbstractVslManageLicenseAction`:
```java
package com.k15t.examples;
public class VslDemoLicenseAction extends com.k15t.vsl.confluence.AbstractVslManageLicenseAction {
    // Required so multiple apps using VSL can coexist.
}
```
`AbstractVslManageLicenseAction` grants access to both Application and System administrators. This can be customized by overriding `AbstractVslManageLicenseAction#isPermitted()`.

It is also possible to re-use an existing xwork package, as long as it uses the validating stack and resides within the `/admin` namespace.

The action parameters need to be customized:

* **selected-web-item-key** should match the module key of the web item (see next section). This parameter is required for highlighting the menu entry when the license UI is displayed.
* **i18n-prefix** should be set to a unique i18n prefix used by the app (see section *Adding i18n for the UI*).

#### Adding a Link to the UI
Then a web item should be registered, so a menu entry appears in the global admin menu:
```xml
    <web-item key="${project.artifactId}-license-item" name="License Configuration Web-Item"
              section="system.admin/${project.artifactId}-global-web-section" weight="20">
        <label key="vsl-demo.vsl.license.ui.navlink"/>
        <link>/admin/plugins/${project.artifactId}/license.action</link>
    </web-item>
```
The `key` attribute needs to match the `selected-web-item-key` parameter in the action definition. The `link` element needs to match the `<package>` `namespace` and `<action>` `name` attributes as defined in the xwork module.


#### Adding i18n for the UI

The UI uses hardcoded i18n keys that will be prefixed with the value from the action parameter `i18n-prefix`.
It's recommended to copy the contents of the [i18n template](vsl-confluence/src/main/resources/com/k15t/vsl/confluence/i18n-template.properties)
into the app's i18n resource bundle and prefix the keys with `i18n-prefix`. See the template file for an example.

The copied i18n values also need to be customized so they match the app's name etc.


## Creating new licenses
Note: This section is not required for integrating VSL into an app.

In order to create new licenses the private key must be available to the program. It's recommended to provide the key in a secure way so only the VSL code can access it but without exposing it to the end user of the program.

A new license can then be generated like this:

1. Create a `UnsignedLicense` containing the license meta information and the policies. Add at least a `ProductKeyPolicy`.
1. Convert the `UnsignedLicense` to a `SignedLicense` by invoking the `sign` method on it.
1. Print the `SignedLicense` to a Hex string by invoking `SignedLicense.toHexString()`.
1. Share the license String with the customer.

Here's a complete example:

```java
public class ExampleLicenseSigner {
    public static void main(String[] args) throws Exception {
        // Provide some license metadata. This will be displayed in the license management UI.
        LicenseInfo licenseInfo = new LicenseInfo(new DateTime("2020-09-01"), "Customer Company", "K15t", "VSL Demo App");
    
        // Add some policies to be validated at runtime.
        List<Policy> policies = Arrays.asList(
                new ProductKeyPolicy("com.k15t.examples.vsl-demo-app"),
                new MaxUsersPolicy(100),
                new ExpirationPolicy(new DateTime("2025-06-30"))
        );
    
        UnsignedLicense unsignedLicense = new UnsignedLicense(licenseInfo, policies);
    
        // Read private key from a file.
        Path privateKeyFile = Paths.get(System.getProperty("user.home"), ".vsl-private-key.txt");
        String privateKeyBase64Encoded = new String(Files.readAllBytes(privateKeyFile), StandardCharsets.UTF_8);
        PrivateKey privateKey = KeyProvider.getPrivateKey(privateKeyBase64Encoded);
    
        // Sign the license.
        SignedLicense signedLicense = unsignedLicense.sign(privateKey);
    
        // Print it as a String. This can be inserted in the license management UI.
        System.out.println(signedLicense.toHexString());
    }
}
``` 
