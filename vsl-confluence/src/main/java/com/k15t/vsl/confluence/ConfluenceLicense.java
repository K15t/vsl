package com.k15t.vsl.confluence;

import com.atlassian.license.License;
import com.k15t.vsl.base.model.HostProductLicense;

import java.time.Instant;
import java.util.Date;


public class ConfluenceLicense implements HostProductLicense {

    private final License confluenceLicense;


    public ConfluenceLicense(License confluenceLicense) {
        this.confluenceLicense = confluenceLicense;
    }


    @Override
    public String getLicenseHolder() {
        return confluenceLicense.getOrganisation();
    }


    @Override
    public Instant getLicenseStartDate() {
        return confluenceLicense.getDateCreated().toInstant();
    }


    @Override
    public Instant getLicenseExpiryDate() {
        Date expiry = confluenceLicense.getExpiryDate();
        return (expiry != null) ? expiry.toInstant() : null;
    }


    @Override
    public String getLicenseType() {
        return confluenceLicense.getLicenseType().getNiceName();
    }


    @Override
    public Instant getLicenseMaintenanceEndDate() {
        return null; // TODO
    }


    @Override
    public int getMaxUsers() {
        return confluenceLicense.getUsers();
    }


    @Override
    public String getSupportEntitlementNumber() {
        return confluenceLicense.getSupportEntitlementNumber();
    }

}
