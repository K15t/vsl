package com.k15t.vsl.confluence;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.k15t.vsl.base.SimpleLicenseService;
import com.k15t.vsl.base.exception.LicenseException;
import com.k15t.vsl.base.model.License;
import com.k15t.vsl.base.model.policies.ExpirationPolicy;
import com.k15t.vsl.base.model.policies.MaxUsersPolicy;
import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.xwork.config.entities.ActionConfig;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * A reusable Xwork action displaying a user interface for VSL license management.
 * <p>
 * To use this UI you need to subclass this action. This is required to properly use it in multiple plugins. The subclass should have a
 * product-specific unique classname. See the VSL integration docs for further information.
 */
public abstract class AbstractVslManageLicenseAction extends ConfluenceActionSupport {

    private static final String VELOCITY_TEMPLATE_STRING;

    private SimpleLicenseService licenseService;

    @SuppressWarnings({"OptionalUsedAsFieldOrParameterType", "OptionalAssignedToNull"})
    private Optional<License> installedLicense = null;
    private String i18nPrefix;

    private String licenseString;


    static {
        URL vmFile = Objects.requireNonNull(AbstractVslManageLicenseAction.class.getClassLoader()
                .getResource("/com/k15t/vsl/confluence/license-ui-internal.vm"));
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(vmFile.openStream(), StandardCharsets.UTF_8))) {
            VELOCITY_TEMPLATE_STRING = reader.lines().collect(Collectors.joining());
        } catch (IOException e) {
            throw new IllegalStateException("Could not load action template resource from classpath.", e);
        }
    }


    @Override
    public boolean isPermitted() {
        return permissionManager.isConfluenceAdministrator(getAuthenticatedUser())
                || permissionManager.isSystemAdministrator(getAuthenticatedUser());
    }


    public String getLicenseString() {
        return licenseString;
    }


    public void setLicenseString(String licenseString) {
        this.licenseString = licenseString;
    }


    public String getVslI18nText(String keyWithoutPrefix) {
        return getText(getVslI18nPrefix() + "." + keyWithoutPrefix);
    }


    private String getVslI18nPrefix() {
        if (i18nPrefix == null) {
            i18nPrefix = Objects.requireNonNull(getActionParameter("i18n-prefix"), "The action definition needs parameter 'i18n-prefix'.");
            if (i18nPrefix.endsWith(".")) {
                i18nPrefix = i18nPrefix.substring(0, i18nPrefix.length() - 1);
            }
        }
        return i18nPrefix;
    }


    private String getActionParameter(String paramName) {
        ActionConfig actionConfig = ServletActionContext.getContext().getActionInvocation().getProxy().getConfig();
        Object o = actionConfig.getParams().get(paramName);
        return (o instanceof String) ? (String) o : null;
    }


    public String getSelectedWebItemKey() {
        return getActionParameter("selected-web-item-key");
    }


    public List<String> getRequiredResourceKeys() {
        List<String> res = new ArrayList<>();

        String webResSpec = getActionParameter("required-web-resource-keys");
        if (webResSpec != null) {
            String[] parts = webResSpec.split(",");
            Collections.addAll(res, parts);
        }

        return res;
    }


    public License getInstalledLicense() {
        return getInstalledLicenseInternal().orElse(null);
    }


    private Optional<License> getInstalledLicenseInternal() {
        //noinspection OptionalAssignedToNull
        if (installedLicense == null) {
            installedLicense = licenseService.getLicense(); // Load it only once during this action invocation.
        }

        return installedLicense;
    }


    public MaxUsersPolicy getMaxUsersPolicy() {
        return getInstalledLicenseInternal().orElseThrow(IllegalStateException::new).getPolicy(MaxUsersPolicy.class);
    }


    public int getConfluenceMaxUsers() {
        return licenseService.getHostProductInfo().getHostProductLicense().getMaxUsers();
    }


    public ExpirationPolicy getExpirationPolicy() {
        return getInstalledLicenseInternal().orElseThrow(IllegalStateException::new).getPolicy(ExpirationPolicy.class);
    }


    public LocalDateTime getNow() {
        return LocalDateTime.now();
    }


    @Override
    public String execute() {
        return SUCCESS;
    }


    public String doUpdate() {
        validate();

        licenseService.storeLicense(licenseString);

        licenseString = "";
        return SUCCESS;
    }


    @Override
    public void validate() {
        try {
            licenseService.validateLicense(licenseString);
        } catch (LicenseException e) {
            addFieldError("licenseString", e.getMessage());
        }
    }


    // This redirection is required to make sure that we always load the template from the current plugin's bundle. Confluence caches action
    // velocity templates by their path (within the plugin bundle) and if there are multiple plugins using VSL this could lead to an action
    // using a template from another plugin. Therefore the cacheable template just contains a proxy call to the internal template.
    public String getVslVelocityTemplateString() {
        return VELOCITY_TEMPLATE_STRING;
    }


    public SimpleLicenseService getSimpleLicenseService() {
        return licenseService;
    }


    public void setSimpleLicenseService(SimpleLicenseService simpleLicenseService) {
        this.licenseService = simpleLicenseService;
    }

}
