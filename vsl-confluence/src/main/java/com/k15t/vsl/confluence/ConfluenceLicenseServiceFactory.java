package com.k15t.vsl.confluence;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.sal.api.ApplicationProperties;
import com.k15t.vsl.base.FileBasedLicenseStore;
import com.k15t.vsl.base.LicenseStore;
import com.k15t.vsl.base.SimpleLicenseService;
import com.k15t.vsl.base.model.LicenseVerifier;

import java.io.File;
import java.nio.file.Path;
import java.security.PublicKey;
import java.time.Instant;
import java.util.Optional;


public class ConfluenceLicenseServiceFactory {

    public static SimpleLicenseService withHomeDirectoryLicenseStore(ApplicationProperties applicationProperties,
            String licensedProductKey, Instant licensedProductBuildDate, PublicKey publicKey) {

        return withHomeDirectoryLicenseStore(applicationProperties, licensedProductKey, licensedProductBuildDate,
                LicenseVerifier.forKey(publicKey));
    }


    public static SimpleLicenseService withHomeDirectoryLicenseStore(ApplicationProperties applicationProperties,
            String licensedProductKey, Instant licensedProductBuildDate, LicenseVerifier licenseVerifier) {

        Path homeDirectory = Optional.ofNullable(applicationProperties.getHomeDirectory())
                .map(File::toPath)
                .filter(Path::isAbsolute)
                .map(Path::normalize)
                .orElseThrow(() -> new IllegalStateException("Could not resolve home directory."));

        Path licensePath = homeDirectory.resolve(licensedProductKey + "-license.xml").normalize();

        if (!licensePath.startsWith(homeDirectory)) {
            throw new IllegalArgumentException("Resolved license file path is outside home directory");
        }

        LicenseStore licenseStore = new FileBasedLicenseStore(licensePath);

        return new SimpleLicenseService(licensedProductKey, licensedProductBuildDate, licenseStore, ConfluenceProductInfo::new,
                licenseVerifier);
    }


    public static SimpleLicenseService withBandanaLicenseStore(BandanaManager bandanaManager, String licensedProductKey,
            Instant licensedProductBuildDate, PublicKey publicKey) {

        return withBandanaLicenseStore(bandanaManager, licensedProductKey, licensedProductBuildDate, LicenseVerifier.forKey(publicKey));
    }


    public static SimpleLicenseService withBandanaLicenseStore(BandanaManager bandanaManager, String licensedProductKey,
            Instant licensedProductBuildDate, LicenseVerifier licenseVerifier) {

        LicenseStore licenseStore = new BandanaBasedLicenseStore(bandanaManager, licensedProductKey);

        return new SimpleLicenseService(licensedProductKey, licensedProductBuildDate, licenseStore, ConfluenceProductInfo::new,
                licenseVerifier);
    }


    public static SimpleLicenseService withCustomLicenseStore(LicenseStore licenseStore, String licensedProductKey,
            Instant licensedProductBuildDate, PublicKey publicKey) {

        return withCustomLicenseStore(licenseStore, licensedProductKey, licensedProductBuildDate,
                LicenseVerifier.forKey(publicKey));
    }


    public static SimpleLicenseService withCustomLicenseStore(LicenseStore licenseStore, String licensedProductKey,
            Instant licensedProductBuildDate, LicenseVerifier licenseVerifier) {

        return new SimpleLicenseService(licensedProductKey, licensedProductBuildDate, licenseStore, ConfluenceProductInfo::new,
                licenseVerifier);
    }

}
