package com.k15t.vsl.confluence;

import com.atlassian.confluence.setup.ConfluenceBootstrapConstants;
import com.atlassian.license.LicenseManager;
import com.k15t.vsl.base.model.HostProductInfo;
import com.k15t.vsl.base.model.HostProductLicense;


public class ConfluenceProductInfo implements HostProductInfo {

    private ConfluenceLicense confluenceLicense;


    public ConfluenceProductInfo() {
        this.confluenceLicense = new ConfluenceLicense(LicenseManager.getInstance().getLicense(
                ConfluenceBootstrapConstants.DEFAULT_LICENSE_REGISTRY_KEY));
    }


    @Override
    public String getDisplayName() {
        return "Confluence";
    }


    @Override
    public HostProductLicense getHostProductLicense() {
        return confluenceLicense;
    }

}
