package com.k15t.vsl.confluence;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;
import com.k15t.vsl.base.LicenseStore;

import java.util.Objects;
import java.util.Optional;


/**
 * {@link LicenseStore} based on Atlassian's Bandana persistence layer.
 */
public class BandanaBasedLicenseStore implements LicenseStore {

    private final BandanaManager bandanaManager;
    protected String bandanaKey;


    public BandanaBasedLicenseStore(BandanaManager bandanaManager, String pluginKey) {
        this.bandanaManager = Objects.requireNonNull(bandanaManager);
        this.bandanaKey = Objects.requireNonNull(pluginKey) + ":vsl-license";
    }


    @Override
    public Optional<String> getLicense() {
        return Optional.ofNullable(bandanaManager.getValue(ConfluenceBandanaContext.GLOBAL_CONTEXT, bandanaKey))
                .filter(String.class::isInstance)
                .map(String.class::cast);
    }


    public void storeLicense(String license) {
        Objects.requireNonNull(license);
        bandanaManager.setValue(ConfluenceBandanaContext.GLOBAL_CONTEXT, bandanaKey, license);
    }


    public void removeLicense() {
        bandanaManager.removeValue(ConfluenceBandanaContext.GLOBAL_CONTEXT, bandanaKey);
    }

}
