package com.k15t.vsl.creator;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.text.WordUtils;

import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.interfaces.RSAPublicKey;


public class KeyGenerator {

    public static void main(String[] args) throws Exception {
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
        kpg.initialize(1024);
        KeyPair keyPair = kpg.genKeyPair();

        PrintWriter pw = new PrintWriter(new OutputStreamWriter(System.out, StandardCharsets.UTF_8));

        pw.println("============================== Public Key ======================================");
        printBase64(keyPair.getPublic(), pw);
        pw.println("================================================================================");
        pw.println();
        pw.println();
        pw.println();
        pw.println("// If public key should be stored in code, hardcode this as a constant:");
        RSAPublicKey rsaPublicKey = (RSAPublicKey) keyPair.getPublic();
        pw.println("private static final RSAPublicKeySpec PUBLIC_KEY_SPEC = new RSAPublicKeySpec(new BigInteger(\""
                + rsaPublicKey.getModulus().toString()+"\"), new BigInteger(\"" + rsaPublicKey.getPublicExponent().toString() + "\"));");
        pw.println();
        pw.println("// Then create a public key from that spec:");
        pw.println("PublicKey publicKey = KeyUtil.createPublicKey(PUBLIC_KEY_SPEC);");
        pw.println();
        pw.println();
        pw.println();
        pw.write("============================== Private Key =====================================\n");
        printBase64(keyPair.getPrivate(), pw);
        pw.write("================================================================================\n");

        pw.flush();
    }



    private static void printBase64(Key key, PrintWriter writer) {
        writer.println(WordUtils.wrap(Base64.encodeBase64String(key.getEncoded()), 80, null, true));
    }

}
