package com.k15t.vsl.creator;

import com.k15t.vsl.base.KeyUtil;
import com.k15t.vsl.base.model.LicenseInfo;
import com.k15t.vsl.base.model.Policy;
import com.k15t.vsl.base.model.SignedLicense;
import com.k15t.vsl.base.model.UnsignedLicense;
import com.k15t.vsl.base.model.policies.ExpirationPolicy;
import com.k15t.vsl.base.model.policies.MaxUsersPolicy;
import com.k15t.vsl.base.model.policies.ProductKeyPolicy;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.PrivateKey;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


public class LicenseCreator {

    public static void main(String[] args) throws Exception {

        // ============================ Customize from here ========================================
        LocalDate today = LocalDate.now();

        LicenseInfo licenseInfo = new LicenseInfo(today, "Customer Company", "K15t GmbH", "VSL Demo App");

        List<Policy> policies = new ArrayList<>();
        policies.add(new ProductKeyPolicy("com.k15t.examples.vsl-demo-app"));

        // Comment this out if no SEN binding is desired.
        // policies.add(new SenPolicy("SEN-123456"));

        // Comment this out if no user limit is desired.
        policies.add(new MaxUsersPolicy(100));

        // Comment this out if no license expiration is desired.
        policies.add(new ExpirationPolicy(today.plusYears(1)));

        // Comment this out if no maintenance expiration is desired.
        // policies.add(new MaintenanceExpirationPolicy(today.plusYears(1)));

        // ============================ Customize until here =======================================


        Path privateKeyFile = Paths.get(System.getProperty("user.home"), ".vsl-private-key.txt");
        String privateKeyText = new String(Files.readAllBytes(privateKeyFile), StandardCharsets.UTF_8);
        PrivateKey privateKey = KeyUtil.createPrivateKey(privateKeyText);

        UnsignedLicense unsignedLicense = new UnsignedLicense(licenseInfo, policies);
        SignedLicense signedLicense = unsignedLicense.sign(privateKey);

        System.out.println();
        System.out.println();
        System.out.println("================================= Signed License =================================");
        System.out.println();

        System.out.println(signedLicense.toHexString());
    }

}
