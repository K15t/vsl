package com.k15t.vsl.base;

import com.k15t.vsl.base.exception.LicenseException;
import com.k15t.vsl.base.exception.LicensePolicyException;
import com.k15t.vsl.base.exception.LicenseSystemException;
import com.k15t.vsl.base.model.HostProductInfo;
import com.k15t.vsl.base.model.HostProductLicense;
import com.k15t.vsl.base.model.License;
import com.k15t.vsl.base.model.LicensedProductInfo;
import com.k15t.vsl.base.model.SignedLicense;
import com.k15t.vsl.base.model.ValidationContext;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;


/**
 * The licenses in this test are created with the {@link TestLicenseSigner}.
 * Use this class to generate new test licenses.
 */
public class SimpleLicenseTest {

    @BeforeClass
    public static void setUp() {
        //setting timezone property in global setup method - because it only works if it is set before another test is run
        //due to some internal initialization
        System.setProperty("user.timezone", "America/Chicago");
    }


    @Test
    public void testValidLicense() {
        LocalDate currentDateTime = LocalDate.parse("2012-12-14");
        LocalDate buildDate = LocalDate.parse("2012-12-14");
        String licenseFile = "/com/k15t/vsl/base/license-with-expiration.xml";

        try {
            getLicenseAndValidate(licenseFile, buildDate, currentDateTime);
        } catch (LicenseSystemException | LicenseException e) {
            fail(e.getMessage());
        }
    }


    @Test
    public void testValidLicenseHexString() {
        LocalDate currentDateTime = LocalDate.parse("2012-12-14");
        LocalDate buildDate = LocalDate.parse("2012-12-14");
        String licenseFile = "/com/k15t/vsl/base/license-with-expiration-hex.txt";

        try {
            getLicenseAndValidate(licenseFile, buildDate, currentDateTime);
        } catch (LicenseSystemException | LicenseException e) {
            fail(e.getMessage());
        }
    }


    @Test
    public void testLicenseWithExpirationExpired() {
        LocalDate currentDateTime = LocalDate.parse("2013-05-01");
        LocalDate buildDate = LocalDate.parse("2012-12-14");
        String licenseFile = "/com/k15t/vsl/base/license-with-expiration.xml";

        try {
            getLicenseAndValidate(licenseFile, buildDate, currentDateTime);
            fail("Missing LicensePolicyException.");
        } catch (LicenseSystemException e) {
            throw new RuntimeException(e);
        } catch (LicenseException e) {
            assertEquals(LicensePolicyException.class, e.getClass());
            LicensePolicyException lpe = (LicensePolicyException) e;
            assertEquals("vsl.policy.error.expired", lpe.getI18nKey());
            assertNotNull(lpe.getArguments());
            assertEquals(1, lpe.getArguments().length);
            assertEquals("2013-04-30", lpe.getArguments()[0]);
        }
    }


    @Test
    public void testLicenseWithMaintenanceExpirationNotExpired() {
        LocalDate currentDateTime = LocalDate.parse("2012-12-14");
        LocalDate buildDate = LocalDate.parse("2013-02-14");
        String licenseFile = "/com/k15t/vsl/base/license-with-maintenance-expiration.xml";

        try {
            getLicenseAndValidate(licenseFile, buildDate, currentDateTime);
        } catch (LicenseSystemException e) {
            throw new RuntimeException(e);
        } catch (LicenseException e) {
            fail("No LicenseException expected.");
        }
    }


    @Test
    public void testLicenseWithCurrentDateAfterMaintenanceExpiration() {
        LocalDate currentDateTime = LocalDate.parse("2013-12-31");
        LocalDate buildDate = LocalDate.parse("2013-02-14");
        String licenseFile = "/com/k15t/vsl/base/license-with-maintenance-expiration.xml";

        try {
            getLicenseAndValidate(licenseFile, buildDate, currentDateTime);
        } catch (LicenseSystemException e) {
            throw new RuntimeException(e);
        } catch (LicenseException e) {
            fail("No LicenseException expected.");
        }
    }


    @Test
    public void testLicenseWithMaintenanceExpirationExpired() {
        LocalDate currentDateTime = LocalDate.parse("2012-12-14");
        LocalDate buildDate = LocalDate.parse("2013-09-30");
        Instant buildDateAsInstant = buildDate.atStartOfDay(ZoneId.systemDefault()).toInstant();
        String licenseFile = "/com/k15t/vsl/base/license-with-maintenance-expiration.xml";

        try {
            getLicenseAndValidate(licenseFile, buildDate, currentDateTime);
            fail("Missing LicensePolicyException.");
        } catch (LicenseSystemException e) {
            throw new RuntimeException(e);
        } catch (LicenseException e) {
            assertEquals(LicensePolicyException.class, e.getClass());
            LicensePolicyException lpe = (LicensePolicyException) e;
            assertEquals("vsl.policy.error.build.out.of.maintenance", lpe.getI18nKey());
            assertNotNull(lpe.getArguments());
            assertEquals(2, lpe.getArguments().length);
            assertEquals("2013-06-30", lpe.getArguments()[0]);
            assertEquals(buildDateAsInstant.toString(), lpe.getArguments()[1]);
        }
    }


    @Test
    public void testLicenseWithMaxUsersExceeded() {
        LocalDate currentDateTime = LocalDate.parse("2012-12-14");
        LocalDate buildDate = LocalDate.parse("2013-02-14");
        String licenseFile = "/com/k15t/vsl/base/license-with-50-users.xml";

        try {
            getLicenseAndValidate(licenseFile, buildDate, currentDateTime);
            fail("Missing LicensePolicyException.");
        } catch (LicenseSystemException e) {
            throw new RuntimeException(e);
        } catch (LicenseException e) {
            assertEquals(LicensePolicyException.class, e.getClass());
            LicensePolicyException lpe = (LicensePolicyException) e;
            assertEquals("vsl.policy.error.license.too.small", lpe.getI18nKey());
            assertNotNull(lpe.getArguments());
            assertEquals(3, lpe.getArguments().length);
            assertEquals("50", lpe.getArguments()[0]);
            assertEquals("Test Host Product", lpe.getArguments()[1]);
            assertEquals("100", lpe.getArguments()[2]);
        }
    }


    @Test
    public void testLicenseWithLicensedProductKeyMismatch() {
        LocalDate currentDateTime = LocalDate.parse("2012-12-14");
        LocalDate buildDate = LocalDate.parse("2013-02-14");
        String licenseFile = "/com/k15t/vsl/base/license-with-different-product-key.xml";

        try {
            getLicenseAndValidate(licenseFile, buildDate, currentDateTime);
            fail("Missing LicensePolicyException.");
        } catch (LicenseSystemException e) {
            throw new RuntimeException(e);
        } catch (LicenseException e) {
            assertEquals(LicensePolicyException.class, e.getClass());
            LicensePolicyException lpe = (LicensePolicyException) e;
            assertEquals("vsl.policy.error.product.key.mismatch", lpe.getI18nKey());
            assertNotNull(lpe.getArguments());
            assertEquals(2, lpe.getArguments().length);
            assertEquals("com.test.bastard.hacker.plugin", lpe.getArguments()[0]);
            assertEquals("com.test.sign.nice-plugin", lpe.getArguments()[1]);
        }
    }


    @Test
    public void testLicenseWithHostProductSenMismatch() {
        LocalDate currentDateTime = LocalDate.parse("2012-12-14");
        LocalDate buildDate = LocalDate.parse("2013-02-14");
        String licenseFile = "/com/k15t/vsl/base/license-with-different-host-product-sen.xml";

        try {
            getLicenseAndValidate(licenseFile, buildDate, currentDateTime);
            fail("Missing LicensePolicyException.");
        } catch (LicenseSystemException e) {
            throw new RuntimeException(e);
        } catch (LicenseException e) {
            assertEquals(LicensePolicyException.class, e.getClass());
            LicensePolicyException lpe = (LicensePolicyException) e;
            assertEquals("vsl.policy.error.sen.mismatch", lpe.getI18nKey());
            assertNotNull(lpe.getArguments());
            assertEquals(3, lpe.getArguments().length);
            assertEquals("THIS-IS-A-DIFFERENT-SEN", lpe.getArguments()[0]);
            assertEquals("Test Host Product", lpe.getArguments()[1]);
            assertEquals("SEN-123", lpe.getArguments()[2]);
        }
    }


    // ------------------------------------ Helper methods ---------------------------------------


    private void getLicenseAndValidate(String licenseFile, LocalDate buildDate, LocalDate currentDateTime)
            throws LicenseSystemException, LicenseException {

        HostProductInfo hostProductInfo = createHostProductInfo();
        LicensedProductInfo licensedProductInfo = LicensedProductInfo.of(
                "com.test.sign.nice-plugin",
                buildDate.atStartOfDay(ZoneId.systemDefault()).toInstant()
        );

        String licenseXml = Optional.ofNullable(this.getClass().getResourceAsStream(licenseFile))
                .map(resource -> {
                    try (BufferedReader reader = new BufferedReader(new InputStreamReader(resource, StandardCharsets.UTF_8))) {
                        return reader.lines().collect(Collectors.joining("\n"));
                    } catch (IOException e) {
                        throw new UncheckedIOException(e);
                    }
                })
                .orElseThrow(IllegalStateException::new);


        LicenseParser parser = new LicenseParser();
        SignedLicense signedLicense = parser.parseSignedLicense(licenseXml);
        signedLicense.verifySignature(TestLicenseSigner.TESTING_PUBLIC_KEY);

        License license = new License(hostProductInfo, licensedProductInfo, signedLicense);

        ValidationContext validationContext = new ValidationContext(currentDateTime.atStartOfDay());
        license.validatePolicies(validationContext);
    }


    private HostProductInfo createHostProductInfo() {
        HostProductLicense license = Mockito.mock(HostProductLicense.class);
        when(license.getLicenseHolder()).thenReturn("test corp");
        when(license.getLicenseStartDate()).thenReturn(LocalDate.of(2012, 1, 1).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
        when(license.getLicenseExpiryDate()).thenReturn(null);
        when(license.getLicenseType()).thenReturn("Commercial");
        when(license.getLicenseMaintenanceEndDate())
                .thenReturn(LocalDate.of(2013, 1, 1).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
        when(license.getMaxUsers()).thenReturn(100);
        when(license.getSupportEntitlementNumber()).thenReturn("SEN-123");

        HostProductInfo info = Mockito.mock(HostProductInfo.class);
        when(info.getDisplayName()).thenReturn("Test Host Product");
        when(info.getHostProductLicense()).thenReturn(license);

        return info;
    }

}
