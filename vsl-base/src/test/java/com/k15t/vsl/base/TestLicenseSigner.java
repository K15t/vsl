package com.k15t.vsl.base;

import com.k15t.vsl.base.model.LicenseInfo;
import com.k15t.vsl.base.model.Policy;
import com.k15t.vsl.base.model.SignedLicense;
import com.k15t.vsl.base.model.UnsignedLicense;
import com.k15t.vsl.base.model.policies.MaintenanceExpirationPolicy;
import com.k15t.vsl.base.model.policies.MaxUsersPolicy;
import com.k15t.vsl.base.model.policies.ProductKeyPolicy;
import com.k15t.vsl.base.model.policies.SenPolicy;
import org.apache.commons.codec.binary.Base64;
import org.junit.Ignore;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.EncodedKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;


@Ignore
public class TestLicenseSigner {

    private static final String TESTING_PUBLIC_KEY_TEXT = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCk9o/kZ5/knhYy5e1YgWjKAq5j"
            + "fC482QFadHMy+gmTyHqFR707fbGRGtxKn/tmr37GpmrvSslRMx20k3ERAWkctu5w0a6ebmpLiAqWxj6nb/v"
            + "ytVtAOO1fb1A9b3ZDrCezA7rcF1vZVfND9L5I6k5xupiS3byazOLk98ttHFTT/wIDAQAB";

    private static final String TESTING_PRIVATE_KEY_TEXT = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAKT2j+Rnn+SeFjL"
            + "l7ViBaMoCrmN8LjzZAVp0czL6CZPIeoVHvTt9sZEa3Eqf+2avfsamau9KyVEzHbSTcREBaRy27nDRrp5uak"
            + "uICpbGPqdv+/K1W0A47V9vUD1vdkOsJ7MDutwXW9lV80P0vkjqTnG6mJLdvJrM4uT3y20cVNP/AgMBAAECg"
            + "YBzQ1SHY2B1fYBFUCLnFIUDoZv4J2ynV3l6mqNjmxHWEN6LAfVW/Bx2Jy6FdgNTXIxYgS92w+qmsfCvm9YE"
            + "69dk+bkwuAeT084ZgG/Mo2N6zRUrX+AOO0MHwn8Gh1ypgr218QiqA9cxqrzIVYLF5ZqIpow2l876ptQHcSk"
            + "xo3TEAQJBANAYzEdXcXaNNnEJc85Ojb5Lz8GftpcBXrUAhW0vJdHp0VSaIgj8ZlOU4x3ENzbFWOzqScEn27"
            + "/fuuCmgaN2BRECQQDK7+HOL2T9CnKSNjIRQEF+ytWNKU0DUtngYNejRCjyh/xB1It5KrvPH6h5h/WRXTANy"
            + "awyjdhVBnvRGllsIggPAkEAvK7vcnRv9b1esqmObm69iOE2lGqbZVohpF6/nFtWoHmwi1RiLeGlan2iXNgP"
            + "9EKGOfLJQzE/yD81NALlovAc8QJBAItuLZLgdV52IynaGqRHpbhb3EXUrpkt/4xufTorxoZ8cBE622kwt0E"
            + "j2zlE9LAferi+a5WAZ7ZjcgbFN9NXvnkCQFFDczS34sZeaYoPuLGdfsPelF8M0hteLoorCLsag3j7xygppa"
            + "KJX15RdBFirnsydsq3ujUeHt5DPHwow7GGxP0=";

    static final PublicKey TESTING_PUBLIC_KEY;
    private static final PrivateKey TESTING_PRIVATE_KEY;


    static {
        try {
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");

            X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(Base64.decodeBase64(TESTING_PUBLIC_KEY_TEXT));
            TESTING_PUBLIC_KEY = keyFactory.generatePublic(publicKeySpec);

            EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(Base64.decodeBase64(TESTING_PRIVATE_KEY_TEXT));
            TESTING_PRIVATE_KEY = keyFactory.generatePrivate(privateKeySpec);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new IllegalStateException(e);
        }
    }


    public static void main(String[] args) {
        LicenseInfo licenseInfo = new LicenseInfo(LocalDate.parse("2012-01-01"), "Test Signing Corp", "Test Licenser", "Some nice plugin");
        List<Policy> policies = Arrays.asList(
                new ProductKeyPolicy("com.test.sign.nice-plugin"),
                new MaxUsersPolicy(100),
                new SenPolicy("THIS-IS-A-DIFFERENT-SEN"),
                new MaintenanceExpirationPolicy(LocalDate.parse("2013-06-30"))
        );

        UnsignedLicense unsignedLicense = new UnsignedLicense(licenseInfo, policies);
        SignedLicense signedLicense = unsignedLicense.sign(TESTING_PRIVATE_KEY);

        System.out.println(signedLicense.toXmlString());
    }

}
