package com.k15t.vsl.base.model.policies;

import com.k15t.vsl.base.exception.LicensePolicyException;
import com.k15t.vsl.base.model.HostProductInfo;
import com.k15t.vsl.base.model.LicensedProductInfo;
import com.k15t.vsl.base.model.ValidationContext;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Objects;


public class MaintenanceExpirationPolicy extends AbstractPolicy {

    public static final String KEY = "MAINTENANCE_EXPIRATION";

    private final LocalDate maintenanceExpiryDate;


    public MaintenanceExpirationPolicy(LocalDate maintenanceExpiryDate) {
        this.maintenanceExpiryDate = Objects.requireNonNull(maintenanceExpiryDate);
    }


    public MaintenanceExpirationPolicy(Element policyEl) {
        maintenanceExpiryDate = LocalDate.from(DateTimeFormatter.ISO_LOCAL_DATE.parse(policyEl.getTextTrim()));
    }


    @Override
    protected void validateImpl(ValidationContext validationContext, HostProductInfo hostProductInfo,
            LicensedProductInfo licensedProductInfo) throws RuntimeException, LicensePolicyException {
        Instant expiry = maintenanceExpiryDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
        if (licensedProductInfo.getBuildDate().isAfter(expiry)) {
            throw new LicensePolicyException("Maintenance expired on " + DateTimeFormatter.ISO_LOCAL_DATE.format(maintenanceExpiryDate)
                    + " but the current product version has been built at a later time ("
                    + DateTimeFormatter.ISO_INSTANT.format(licensedProductInfo.getBuildDate()) + ").",
                    "vsl.policy.error.build.out.of.maintenance", DateTimeFormatter.ISO_LOCAL_DATE.format(maintenanceExpiryDate),
                    DateTimeFormatter.ISO_INSTANT.format(licensedProductInfo.getBuildDate()));
        }
    }


    public LocalDate getMaintenanceExpiryDate() {
        return this.maintenanceExpiryDate;
    }


    @Override
    public Element toElement() {
        Element el = DocumentHelper.createElement("policy");
        el.addAttribute("type", KEY);
        el.setText(DateTimeFormatter.ISO_LOCAL_DATE.format(maintenanceExpiryDate));
        return el;
    }


    @Override
    public String toString() {
        return "MaintenanceExpirationPolicy [maintenanceExpiryDate=" + DateTimeFormatter.ISO_LOCAL_DATE.format(this.maintenanceExpiryDate)
                + "]";
    }

}
