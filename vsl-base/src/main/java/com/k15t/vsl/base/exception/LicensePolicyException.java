package com.k15t.vsl.base.exception;

public class LicensePolicyException extends LicenseException {

    public LicensePolicyException(String defaultMessage, Throwable e, String i18nKey, String... arguments) {
        super(defaultMessage, e, i18nKey, arguments);
    }


    public LicensePolicyException(Throwable e) {
        super(e);
    }


    public LicensePolicyException(String defaultMessage, String i18nKey, String... arguments) {
        super(defaultMessage, i18nKey, arguments);
    }

}
