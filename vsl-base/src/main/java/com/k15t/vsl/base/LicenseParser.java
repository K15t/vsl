package com.k15t.vsl.base;

import com.k15t.vsl.base.exception.InvalidLicenseException;
import com.k15t.vsl.base.exception.LicenseException;
import com.k15t.vsl.base.model.LicenseInfo;
import com.k15t.vsl.base.model.Policy;
import com.k15t.vsl.base.model.SignedLicense;
import com.k15t.vsl.base.model.policies.ExpirationPolicy;
import com.k15t.vsl.base.model.policies.MaintenanceExpirationPolicy;
import com.k15t.vsl.base.model.policies.MaxRemoteEndpointsPolicy;
import com.k15t.vsl.base.model.policies.MaxUsersPolicy;
import com.k15t.vsl.base.model.policies.ProductKeyPolicy;
import com.k15t.vsl.base.model.policies.SenPolicy;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.xml.sax.SAXException;

import java.io.StringReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class LicenseParser {

    public static final String LICENSE_HEADER = Hex.encodeHexString("VSL2.XML:".getBytes(StandardCharsets.UTF_8));

    private static final Map<String, Class<? extends Policy>> availablePolicies = new HashMap<>();


    static {
        availablePolicies.put(ExpirationPolicy.KEY, ExpirationPolicy.class);
        availablePolicies.put(MaintenanceExpirationPolicy.KEY, MaintenanceExpirationPolicy.class);
        availablePolicies.put(MaxUsersPolicy.KEY, MaxUsersPolicy.class);
        availablePolicies.put(ProductKeyPolicy.KEY, ProductKeyPolicy.class);
        availablePolicies.put(SenPolicy.KEY, SenPolicy.class);
        availablePolicies.put(MaxRemoteEndpointsPolicy.KEY, MaxRemoteEndpointsPolicy.class);
    }


    public SignedLicense parseSignedLicense(String licenseData) throws LicenseException {
        try {
            licenseData = StringUtils.trimToEmpty(licenseData);

            if (StringUtils.startsWithIgnoreCase(licenseData, LICENSE_HEADER)) {
                licenseData = StringUtils.removeStartIgnoreCase(licenseData, LICENSE_HEADER);
                licenseData = StringUtils.deleteWhitespace(licenseData);
                licenseData = new String(Hex.decodeHex(licenseData.toCharArray()), StandardCharsets.UTF_8);
            }

            SAXReader reader = new SAXReader();
            reader.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
            reader.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            reader.setFeature("http://xml.org/sax/features/external-general-entities", false);
            reader.setFeature("http://xml.org/sax/features/external-parameter-entities", false);

            Document document = reader.read(new StringReader(licenseData));

            LicenseInfo licenseInfo = new LicenseInfo(document.getRootElement().element("info"));

            Element policiesEl = document.getRootElement().element("policies");
            if (policiesEl == null) {
                throw new InvalidLicenseException("Invalid License: no policies specified.",
                        "vsl.error.no.policies.specified");
            }

            @SuppressWarnings("unchecked") List<Element> policyEls = (List<Element>) policiesEl.elements("policy");
            if (policyEls == null) {
                throw new InvalidLicenseException("Invalid License: no policies specified.",
                        "vsl.error.no.policies.specified");
            }

            List<Policy> policies = new ArrayList<>();
            for (Element policyEl : policyEls) {
                policies.add(createPolicy(policyEl));
            }

            String signature = document.getRootElement().elementTextTrim("signature");

            if (StringUtils.isBlank(signature)) {
                throw new IllegalArgumentException("Not a signed license");
            }

            return new SignedLicense(licenseInfo, policies, signature);
        } catch (RuntimeException | DocumentException e) {
            throw new LicenseException("Exception loading license from XML.", e, "vsl.error.parse");
        } catch (DecoderException e) {
            throw new LicenseException("An error occurred while decoding the license text.", e, "vsl.error.parse");
        } catch (SAXException e) {
            throw new IllegalStateException("Could not configure XML parser.", e);
        }
    }


    private Policy createPolicy(Element policyElement) throws LicenseException {
        try {
            String type = policyElement.attributeValue("type");
            if (type == null) {
                throw new InvalidLicenseException("No type specified for policy.",
                        "vsl.error.invalid.policy.spec.no.type");
            }

            Class<? extends Policy> policy = availablePolicies.get(type);
            if (policy == null) {
                throw new InvalidLicenseException("Invalid type '" + type + "'",
                        "vsl.error.invalid.policy.spec.invalid.class", type);
            }

            Constructor<? extends Policy> constructor = policy.getConstructor(Element.class);
            return constructor.newInstance(policyElement);
        } catch (InvocationTargetException | NoSuchMethodException | InstantiationException | IllegalAccessException e) {
            throw new LicenseException(e);
        }
    }

}
