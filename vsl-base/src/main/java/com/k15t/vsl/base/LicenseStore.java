package com.k15t.vsl.base;

import java.util.Optional;


/**
 * Responsible for storing and accessing licenses that users install.
 */
public interface LicenseStore {

    Optional<String> getLicense();


    void storeLicense(String license);


    void removeLicense();

}
