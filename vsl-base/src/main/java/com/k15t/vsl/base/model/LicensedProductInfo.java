package com.k15t.vsl.base.model;

import java.time.Instant;


public interface LicensedProductInfo {

    static LicensedProductInfo of(String productKey, Instant buildDate) {
        return new LicensedProductInfo() {
            @Override
            public String getProductKey() {
                return productKey;
            }


            @Override
            public Instant getBuildDate() {
                return buildDate;
            }
        };
    }


    String getProductKey();


    Instant getBuildDate();

}
