package com.k15t.vsl.base;

import com.k15t.vsl.base.exception.LicenseSystemException;
import org.apache.commons.codec.binary.Base64;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.EncodedKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPublicKeySpec;


public class KeyUtil {

    public static PublicKey createPublicKey(RSAPublicKeySpec keySpec) {
        try {
            return KeyFactory.getInstance("RSA").generatePublic(keySpec);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new IllegalArgumentException(e);
        }
    }


    public static PrivateKey createPrivateKey(String base64EncodedPrivateRsaKey) {
        try {
            KeyFactory privateKeyFactory = KeyFactory.getInstance("RSA");
            EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(Base64.decodeBase64(base64EncodedPrivateRsaKey));
            return privateKeyFactory.generatePrivate(privateKeySpec);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new LicenseSystemException(e);
        }
    }

}
