package com.k15t.vsl.base.exception;


public class InvalidLicenseException extends LicenseException {

    public InvalidLicenseException(String defaultMessage, Throwable e, String i18nKey, String... arguments) {
        super(defaultMessage, e, i18nKey, arguments);
    }


    public InvalidLicenseException(Throwable e) {
        super(e);
    }


    public InvalidLicenseException(String defaultMessage, String i18nKey, String... arguments) {
        super(defaultMessage, i18nKey, arguments);
    }

}
