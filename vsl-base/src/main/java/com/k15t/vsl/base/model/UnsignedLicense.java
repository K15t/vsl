package com.k15t.vsl.base.model;

import com.k15t.vsl.base.exception.LicenseSystemException;
import org.apache.commons.codec.binary.Base64;

import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.List;


/**
 * Encapsulates <b>unsigned</b> license metadata and policies. This should not be used for license checks because it cannot be trusted as it
 * doesn't contain a signature. Instances must be converted to a {@link SignedLicense} by invoking the {@link #sign(PrivateKey)} method.
 *
 * @see SignedLicense
 */
public class UnsignedLicense extends RawLicense {

    public UnsignedLicense(LicenseInfo licenseInfo, List<Policy> policies) {
        super(licenseInfo, policies);
    }


    public SignedLicense sign(PrivateKey privateKey) {
        try {
            String licenseHash = createLicenseHash();

            Signature sig = Signature.getInstance(Constants.PKIALG);
            sig.initSign(privateKey);
            sig.update(licenseHash.getBytes(StandardCharsets.UTF_8));
            String signature = Base64.encodeBase64String(sig.sign());

            return new SignedLicense(getLicenseInfo(), new ArrayList<>(getPolicies()), signature);
        } catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException e) {
            throw new LicenseSystemException(e);
        }
    }

}
