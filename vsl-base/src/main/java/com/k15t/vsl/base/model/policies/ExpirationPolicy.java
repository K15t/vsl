package com.k15t.vsl.base.model.policies;

import com.k15t.vsl.base.exception.LicensePolicyException;
import com.k15t.vsl.base.model.HostProductInfo;
import com.k15t.vsl.base.model.LicensedProductInfo;
import com.k15t.vsl.base.model.ValidationContext;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;


public class ExpirationPolicy extends AbstractPolicy {

    public static final String KEY = "EXPIRATION";

    private final LocalDate expiryDate;


    public ExpirationPolicy(LocalDate expiryDate) {
        this.expiryDate = Objects.requireNonNull(expiryDate);
    }


    public ExpirationPolicy(Element policyEl) {
        expiryDate = LocalDate.from(DateTimeFormatter.ISO_LOCAL_DATE.parse(policyEl.getTextTrim()));
    }


    @Override
    protected void validateImpl(ValidationContext validationContext, HostProductInfo hostProductInfo,
            LicensedProductInfo licensedProductInfo) throws RuntimeException, LicensePolicyException {

        if (validationContext.getCurrentDateTime().isAfter(expiryDate.atStartOfDay())) {
            throw new LicensePolicyException("License expired on " + expiryDate,
                    "vsl.policy.error.expired", DateTimeFormatter.ISO_LOCAL_DATE.format(expiryDate));
        }
    }


    public LocalDate getExpiryDate() {
        return this.expiryDate;
    }


    @Override
    public Element toElement() {
        Element el = DocumentHelper.createElement("policy");
        el.addAttribute("type", KEY);
        el.setText(DateTimeFormatter.ISO_LOCAL_DATE.format(expiryDate));
        return el;
    }


    @Override
    public String toString() {
        return "ExpirationPolicy{" + "expiryDate=" + DateTimeFormatter.ISO_LOCAL_DATE.format(expiryDate) + '}';
    }

}
