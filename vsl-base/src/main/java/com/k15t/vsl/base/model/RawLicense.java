package com.k15t.vsl.base.model;

import org.apache.commons.codec.digest.DigestUtils;

import java.util.Collections;
import java.util.List;
import java.util.Objects;


/**
 * Base container for license metadata and policies with their values.
 *
 * @see UnsignedLicense
 * @see SignedLicense
 */
abstract class RawLicense {

    private final LicenseInfo licenseInfo;
    private final List<Policy> policies;


    RawLicense(LicenseInfo licenseInfo, List<Policy> policies) {
        this.licenseInfo = Objects.requireNonNull(licenseInfo);
        this.policies = Collections.unmodifiableList(Objects.requireNonNull(policies));
    }


    public LicenseInfo getLicenseInfo() {
        return this.licenseInfo;
    }


    public List<Policy> getPolicies() {
        return this.policies;
    }


    protected String createLicenseHash() {
        String text = "License{licenseInfo=" + licenseInfo + ", policies=" + policies + '}';
        return DigestUtils.md5Hex(text);
    }

}
