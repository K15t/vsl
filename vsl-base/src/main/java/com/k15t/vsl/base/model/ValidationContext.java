package com.k15t.vsl.base.model;

import java.time.LocalDateTime;


public class ValidationContext {

    private final LocalDateTime currentDateTime;


    public ValidationContext() {
        currentDateTime = LocalDateTime.now();
    }


    public ValidationContext(LocalDateTime currentDateTime) {
        this.currentDateTime = currentDateTime;
    }


    public LocalDateTime getCurrentDateTime() {
        return this.currentDateTime;
    }

}
