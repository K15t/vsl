package com.k15t.vsl.base.model.policies;

import com.k15t.vsl.base.exception.LicensePolicyException;
import com.k15t.vsl.base.model.HostProductInfo;
import com.k15t.vsl.base.model.LicensedProductInfo;
import com.k15t.vsl.base.model.ValidationContext;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;


public class MaxUsersPolicy extends AbstractPolicy {

    public static final String KEY = "MAX_USERS";

    private final int maxUsers;


    public MaxUsersPolicy(int maxUsers) {
        this.maxUsers = maxUsers;
    }


    public MaxUsersPolicy(Element policyEl) {
        maxUsers = Integer.parseInt(policyEl.getTextTrim());
    }


    @Override
    protected void validateImpl(ValidationContext validationContext, HostProductInfo hostProductInfo,
            LicensedProductInfo licensedProductInfo) throws RuntimeException, LicensePolicyException {

        int hostLicenseUsers = hostProductInfo.getHostProductLicense().getMaxUsers();

        if (hostLicenseUsers > (maxUsers + 2)) {
            throw new LicensePolicyException("This license allows " + maxUsers + " users but the "
                    + hostProductInfo.getDisplayName() + " license allows " + hostLicenseUsers + " users.",
                    "vsl.policy.error.license.too.small", Integer.toString(maxUsers), hostProductInfo
                            .getDisplayName(), Integer.toString(hostLicenseUsers));
        }
    }


    public int getMaxUsers() {
        return this.maxUsers;
    }


    @Override
    public Element toElement() {
        Element el = DocumentHelper.createElement("policy");
        el.addAttribute("type", KEY);
        el.setText(maxUsers + "");
        return el;
    }


    @Override
    public String toString() {
        return "MaxUsersPolicy{" + "maxUsers=" + maxUsers + '}';
    }

}
