package com.k15t.vsl.base.model.policies;

import com.k15t.vsl.base.exception.LicensePolicyException;
import com.k15t.vsl.base.model.HostProductInfo;
import com.k15t.vsl.base.model.LicensedProductInfo;
import com.k15t.vsl.base.model.Policy;
import com.k15t.vsl.base.model.ValidationContext;


public abstract class AbstractPolicy implements Policy {

    @Override
    public final void validate(ValidationContext validationContext, HostProductInfo hostProductInfo,
            LicensedProductInfo licensedProductInfo) throws LicensePolicyException {

        try {
            validateImpl(validationContext, hostProductInfo, licensedProductInfo);
        } catch (RuntimeException e) {
            throw new LicensePolicyException(e);
        }
    }


    protected abstract void validateImpl(ValidationContext validationContext,
            HostProductInfo hostProductInfo, LicensedProductInfo licensedProductInfo)
            throws RuntimeException, LicensePolicyException;

}
