package com.k15t.vsl.base.model;


public interface HostProductInfo {

    String getDisplayName();

    HostProductLicense getHostProductLicense();

}
