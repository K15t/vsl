package com.k15t.vsl.base;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;
import java.util.Optional;


/**
 * {@link LicenseStore} based on a file in the filesystem. The caller is responsible to ensure that the referenced file may be processed.
 */
public class FileBasedLicenseStore implements LicenseStore {

    private final Path licenseFile;


    public FileBasedLicenseStore(Path licenseFile) {
        this.licenseFile = Objects.requireNonNull(licenseFile);
    }


    @Override
    public Optional<String> getLicense() {
        return Optional.of(licenseFile)
                .filter(Files::exists)
                .map(file -> {
                    try {
                        return new String(Files.readAllBytes(file), StandardCharsets.UTF_8);
                    } catch (IOException e) {
                        throw new UncheckedIOException(e);
                    }
                });
    }


    @Override
    public void storeLicense(String license) {
        try {
            Files.write(licenseFile, license.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }


    @Override
    public void removeLicense() {
        try {
            Files.deleteIfExists(licenseFile);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

}
