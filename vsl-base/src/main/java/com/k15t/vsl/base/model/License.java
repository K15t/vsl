package com.k15t.vsl.base.model;

import com.k15t.vsl.base.exception.LicensePolicyException;

import java.util.List;


/**
 * An app license that can be validated in the scope of license checks in an app.
 */
public class License {

    private final SignedLicense signedLicense;
    private final HostProductInfo hostProductInfo;
    private final LicensedProductInfo licensedProductInfo;


    public License(HostProductInfo hostProductInfo, LicensedProductInfo licensedProductInfo, SignedLicense signedLicense) {
        this.hostProductInfo = hostProductInfo;
        this.licensedProductInfo = licensedProductInfo;
        this.signedLicense = signedLicense;
    }


    public LicenseInfo getLicenseInfo() {
        return signedLicense.getLicenseInfo();
    }


    @SuppressWarnings("unchecked")
    public <T extends Policy> T getPolicy(Class<T> policyClass) {
        for (Policy policy : signedLicense.getPolicies()) {
            if (policyClass.isInstance(policy)) {
                return (T) policy;
            }
        }

        return null;
    }


    public List<Policy> getPolicies() {
        return signedLicense.getPolicies();
    }


    public void validatePolicies() throws LicensePolicyException {
        validatePolicies(new ValidationContext());
    }


    public void validatePolicies(ValidationContext validationContext) throws LicensePolicyException {
        for (Policy policy : signedLicense.getPolicies()) {
            policy.validate(validationContext, hostProductInfo, licensedProductInfo);
        }
    }

}
