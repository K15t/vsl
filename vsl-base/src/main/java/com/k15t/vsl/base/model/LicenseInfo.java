package com.k15t.vsl.base.model;

import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


public class LicenseInfo {

    private final LocalDate creation;
    private final String holder;
    private final String creator;
    private final String product;


    public LicenseInfo(LocalDate creation, String holder, String creator, String product) {
        this.creation = creation;
        this.holder = holder;
        this.creator = creator;
        this.product = product;
    }


    public LicenseInfo(Element infoEl) {
        creation = LocalDate.from(DateTimeFormatter.ISO_LOCAL_DATE.parse(infoEl.elementText("creation")));
        holder = infoEl.elementText("holder");
        creator = infoEl.elementText("creator");
        product = infoEl.elementText("product");
    }


    public Element toElement() {
        Element infoEl = DocumentHelper.createElement("info");

        infoEl.addElement("creation").setText(DateTimeFormatter.ISO_LOCAL_DATE.format(creation));
        infoEl.addElement("holder").setText(holder);
        infoEl.addElement("creator").setText(creator);
        infoEl.addElement("product").setText(product);

        return infoEl;
    }


    @Override
    public String toString() {
        return "LicenseInfo{" + "creation=" + DateTimeFormatter.ISO_LOCAL_DATE.format(creation) + ", holder='" + holder + '\''
                + ", creator='" + creator + '\'' + ", product='" + product + '\'' + '}';
    }


    public LocalDate getCreation() {
        return this.creation;
    }


    public String getHolder() {
        return this.holder;
    }


    public String getCreator() {
        return this.creator;
    }


    public String getProduct() {
        return this.product;
    }

}
