package com.k15t.vsl.base.model;

import com.k15t.vsl.base.LicenseParser;
import com.k15t.vsl.base.exception.InvalidLicenseException;
import com.k15t.vsl.base.exception.LicenseException;
import com.k15t.vsl.base.exception.LicenseSystemException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.text.WordUtils;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import java.io.IOException;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.util.List;
import java.util.Objects;


/**
 * Encapsulates signed license metadata and policies. This is the object representation of the license string shared with customers.
 * For license checks within an app {@link License} should be used, which also contains the context information required for validation
 * against a host product's properties.
 *
 * @see UnsignedLicense
 * @see License
 */
public class SignedLicense extends RawLicense {

    private final String signature;


    public SignedLicense(LicenseInfo licenseInfo, List<Policy> policies, String signature) {
        super(licenseInfo, policies);
        this.signature = Objects.requireNonNull(signature);
    }


    public void verifySignature(PublicKey publicKey) throws LicenseException {
        try {
            String licenseHash = createLicenseHash();

            Signature sig = Signature.getInstance(Constants.PKIALG);
            sig.initVerify(publicKey);
            sig.update(licenseHash.getBytes(StandardCharsets.UTF_8));
            if (!sig.verify(Base64.decodeBase64(signature))) {
                throw new InvalidLicenseException("Signature is invalid.", "vsl.error.invalid.signature");
            }
        } catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException e) {
            throw new LicenseSystemException(e);
        }
    }


    public String toXmlString() {
        try {
            StringWriter stringWriter = new StringWriter();
            OutputFormat format = OutputFormat.createCompactFormat();
            XMLWriter writer = new XMLWriter(stringWriter, format);
            writer.write(toDocument());
            writer.close();
            return stringWriter.toString();
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("WTF, UTF-8 not supported?");
        } catch (IOException e) {
            throw new LicenseSystemException(e);
        }
    }


    private Document toDocument() {
        Document document = DocumentHelper.createDocument();
        Element licenseEl = document.addElement("license");

        licenseEl.add(getLicenseInfo().toElement());
        Element policiesEl = licenseEl.addElement("policies");
        for (Policy policy : getPolicies()) {
            policiesEl.add(policy.toElement());
        }
        licenseEl.addElement("signature").setText(signature);

        return document;
    }


    public String toHexString() {
        String encoded = LicenseParser.LICENSE_HEADER + Hex.encodeHexString(toXmlString().getBytes(StandardCharsets.UTF_8));
        return WordUtils.wrap(encoded, 80, null, true);
    }


    @Override
    public String toString() {
        return toHexString();
    }

}
