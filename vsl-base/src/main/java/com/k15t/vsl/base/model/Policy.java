package com.k15t.vsl.base.model;

import com.k15t.vsl.base.exception.LicensePolicyException;
import org.dom4j.Element;


public interface Policy {


    void validate(ValidationContext validationContext, HostProductInfo hostProductInfo,
            LicensedProductInfo licensedProductInfo) throws LicensePolicyException;


    Element toElement();

}
