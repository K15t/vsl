package com.k15t.vsl.base.model.policies;

import com.k15t.vsl.base.exception.LicensePolicyException;
import com.k15t.vsl.base.model.HostProductInfo;
import com.k15t.vsl.base.model.LicensedProductInfo;
import com.k15t.vsl.base.model.ValidationContext;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import java.util.Objects;


public class ProductKeyPolicy extends AbstractPolicy {

    public static final String KEY = "PRODUCT_KEY";

    private final String productKey;


    public ProductKeyPolicy(String productKey) {
        this.productKey = Objects.requireNonNull(productKey);
    }


    public ProductKeyPolicy(Element policyEl) {
        productKey = policyEl.getTextTrim();
    }


    @Override
    protected void validateImpl(ValidationContext validationContext, HostProductInfo hostProductInfo,
            LicensedProductInfo licensedProductInfo) throws RuntimeException, LicensePolicyException {

        if (!StringUtils.equalsIgnoreCase(productKey, licensedProductInfo.getProductKey())) {
            throw new LicensePolicyException("This license has been issued for '" + productKey
                    + "' but is validated in the context of '" + licensedProductInfo.getProductKey() + "'.",
                    "vsl.policy.error.product.key.mismatch", productKey, licensedProductInfo
                            .getProductKey());
        }
    }


    public String getProductKey() {
        return this.productKey;
    }


    @Override
    public Element toElement() {
        Element el = DocumentHelper.createElement("policy");
        el.addAttribute("type", KEY);
        el.setText(productKey + "");
        return el;
    }


    @Override
    public String toString() {
        return "ProductKeyPolicy{" + "productKey='" + productKey + '\'' + '}';
    }

}
