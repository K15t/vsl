package com.k15t.vsl.base.exception;


public class LicenseSystemException extends RuntimeException {

    private String i18nKey;
    private String[] arguments;


    public LicenseSystemException(String defaultMessage, String i18nKey, String... arguments) {
        super(defaultMessage);
        this.i18nKey = i18nKey;
        this.arguments = arguments;
    }


    public LicenseSystemException(Throwable e) {
        super("An unexpected error occurred related to the license.", e);
        this.i18nKey = "vsl.error.unexpected";
    }


    public LicenseSystemException(String defaultMessage, Throwable e, String i18nKey, String... arguments) {
        super(defaultMessage, e);
        this.i18nKey = i18nKey;
        this.arguments = arguments;
    }


    public String getI18nKey() {
        return this.i18nKey;
    }


    public String[] getArguments() {
        return this.arguments;
    }

}
