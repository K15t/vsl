package com.k15t.vsl.base.model;

import com.k15t.vsl.base.exception.LicenseException;

import java.security.PublicKey;


public interface LicenseVerifier {

    /**
     * Verifies the signature of the license. Implementations should invoke {@link SignedLicense#verifySignature(PublicKey)} and may
     * forward any exception thrown by this method.
     */
    void verify(SignedLicense license) throws LicenseException;


    static LicenseVerifier forKey(PublicKey publicKey) {
        return license -> license.verifySignature(publicKey);
    }

}
