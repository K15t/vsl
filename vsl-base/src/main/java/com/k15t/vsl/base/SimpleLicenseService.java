package com.k15t.vsl.base;

import com.k15t.vsl.base.exception.LicenseException;
import com.k15t.vsl.base.model.HostProductInfo;
import com.k15t.vsl.base.model.License;
import com.k15t.vsl.base.model.LicenseVerifier;
import com.k15t.vsl.base.model.LicensedProductInfo;
import com.k15t.vsl.base.model.SignedLicense;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.PublicKey;
import java.time.Instant;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;


/**
 * Service for retrieving and managing VSL licenses.
 * <p>
 * Use a spring factory bean and the corresponding service factory (for example ConfluenceLicenseServiceFactory) to create an instance of
 * this class and make it available in the spring context. See the VSL integration docs for further information.
 */
public class SimpleLicenseService {

    private static final Logger logger = LoggerFactory.getLogger(SimpleLicenseService.class);

    private final LicensedProductInfo licensedProductInfo;
    private final LicenseStore licenseStore;
    private final Supplier<HostProductInfo> hostProductInfoProvider;
    private final LicenseParser licenseParser;
    private final LicenseVerifier licenseVerifier;
    private final ObjectCache<Optional<License>> licenseCache;


    public SimpleLicenseService(String licensedProductKey, Instant licensedProductBuildDate, LicenseStore licenseStore,
            Supplier<HostProductInfo> hostProductInfoProvider, PublicKey verificationKey) {

        this(licensedProductKey,licensedProductBuildDate,licenseStore, hostProductInfoProvider, LicenseVerifier.forKey(verificationKey));
    }


    public SimpleLicenseService(String licensedProductKey, Instant licensedProductBuildDate, LicenseStore licenseStore,
            Supplier<HostProductInfo> hostProductInfoProvider, LicenseVerifier licenseVerifier) {

        this.licensedProductInfo = LicensedProductInfo.of(licensedProductKey, licensedProductBuildDate);
        this.licenseStore = licenseStore;
        this.hostProductInfoProvider = hostProductInfoProvider;
        this.licenseParser = new LicenseParser();
        this.licenseVerifier = licenseVerifier;

        Supplier<Optional<License>> loader = () -> licenseStore.getLicense()
                .map(licenseText -> {
                    try {
                        SignedLicense signedLicense = licenseParser.parseSignedLicense(licenseText);
                        licenseVerifier.verify(signedLicense);
                        return new License(hostProductInfoProvider.get(), licensedProductInfo, signedLicense);
                    } catch (com.k15t.vsl.base.exception.LicenseException e) {
                        logger.warn("Could not parse and verify license for '{}'.", licensedProductKey, e);
                        return null;
                    }
                });

        licenseCache = new ObjectCache<>(loader, 1, TimeUnit.MINUTES);
    }


    public Optional<License> getLicense() {
        return licenseCache.get();
    }


    public HostProductInfo getHostProductInfo() {
        return this.hostProductInfoProvider.get();
    }


    public void storeLicense(String license) {
        licenseStore.storeLicense(license);
        licenseCache.flush();
    }


    public void removeLicense() {
        licenseStore.removeLicense();
        licenseCache.flush();
    }


    public void validateLicense(String license) throws LicenseException {
        SignedLicense signedLicense = licenseParser.parseSignedLicense(license);
        licenseVerifier.verify(signedLicense);
        License candidate = new License(hostProductInfoProvider.get(), licensedProductInfo, signedLicense);
        candidate.validatePolicies();
    }


    private static class ObjectCache<T> implements Supplier<T> {

        private final Object mutex = new Object();
        private long updatedAt = 0;
        private T cached = null;
        private final Supplier<T> loader;
        private final long ttlNanos;


        private ObjectCache(Supplier<T> loader, long ttl, TimeUnit unit) {
            this.loader = loader;
            this.ttlNanos = unit.toNanos(ttl);
        }


        @Override
        public T get() {
            synchronized (mutex) {
                long now = System.nanoTime();
                if (now > updatedAt + ttlNanos) {
                    cached = null;
                }
                if (cached == null) {
                    cached = loader.get();
                    updatedAt = now;
                }
                return cached;
            }
        }


        public void flush() {
            synchronized (mutex) {
                cached = null;
            }
        }

    }

}
