package com.k15t.vsl.base.model.policies;

import com.k15t.vsl.base.exception.LicensePolicyException;
import com.k15t.vsl.base.model.HostProductInfo;
import com.k15t.vsl.base.model.LicensedProductInfo;
import com.k15t.vsl.base.model.ValidationContext;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import java.util.Objects;


public class SenPolicy extends AbstractPolicy {

    public static final String KEY = "SEN";

    private final String sen;


    public SenPolicy(String sen) {
        this.sen = Objects.requireNonNull(sen);
    }


    public SenPolicy(Element policyEl) {
        sen = policyEl.getTextTrim();
    }


    @Override
    protected void validateImpl(ValidationContext validationContext, HostProductInfo hostProductInfo,
            LicensedProductInfo licensedProductInfo) throws RuntimeException, LicensePolicyException {

        String hostSen = hostProductInfo.getHostProductLicense().getSupportEntitlementNumber();

        if (!StringUtils.equalsIgnoreCase(sen, hostSen)) {
            throw new LicensePolicyException("This license is bound to the SEN '" + sen + "' but the "
                    + hostProductInfo.getDisplayName() + " SEN is '" + hostSen + "'.",
                    "vsl.policy.error.sen.mismatch", sen, hostProductInfo.getDisplayName(), hostSen);
        }
    }


    public String getSen() {
        return this.sen;
    }


    @Override
    public Element toElement() {
        Element el = DocumentHelper.createElement("policy");
        el.addAttribute("type", KEY);
        el.setText(sen + "");
        return el;
    }


    @Override
    public String toString() {
        return "SenPolicy{" + "sen='" + sen + '\'' + '}';
    }

}
