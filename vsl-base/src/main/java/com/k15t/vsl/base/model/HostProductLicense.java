package com.k15t.vsl.base.model;

import java.time.Instant;


public interface HostProductLicense {

    String getLicenseHolder();


    Instant getLicenseStartDate();


    Instant getLicenseExpiryDate();


    String getLicenseType();


    Instant getLicenseMaintenanceEndDate();


    int getMaxUsers();


    String getSupportEntitlementNumber();

}
