package com.k15t.vsl.base.model.policies;

import com.k15t.vsl.base.model.HostProductInfo;
import com.k15t.vsl.base.model.LicensedProductInfo;
import com.k15t.vsl.base.model.ValidationContext;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * This policy represents the amount of maximum backbone remote endpoints.
 */
public class MaxRemoteEndpointsPolicy extends AbstractPolicy {

    private static final Logger log = LoggerFactory.getLogger(MaxRemoteEndpointsPolicy.class);
    public static final String KEY = "MAX_REMOTE_ENDPOINTS";

    private int maxRemoteEndpoints;


    public MaxRemoteEndpointsPolicy(int maxRemoteEndpoints) {
        this.maxRemoteEndpoints = maxRemoteEndpoints;
    }


    public MaxRemoteEndpointsPolicy(Element policyEl) {
        try {
            maxRemoteEndpoints = Integer.parseInt(policyEl.getTextTrim());
        } catch (NumberFormatException e) {
            log.error("License does not contain a valid integer for the maximum remote endpoints.", e);
            maxRemoteEndpoints = 0;
        }
    }


    @Override
    protected void validateImpl(ValidationContext validationContext, HostProductInfo hostProductInfo,
            LicensedProductInfo licensedProductInfo) {
        //The validation of remote endpoints is done dynamically
    }


    public int getMaxRemoteEndpoints() {
        return this.maxRemoteEndpoints;
    }


    @Override
    public Element toElement() {
        Element el = DocumentHelper.createElement("policy");
        el.addAttribute("type", KEY);
        el.setText(String.valueOf(maxRemoteEndpoints));
        return el;
    }


    @Override
    public String toString() {
        return "MaxRemoteEndpointsPolicy{" + "maxRemoteEndpoints=" + maxRemoteEndpoints + '}';
    }
}
